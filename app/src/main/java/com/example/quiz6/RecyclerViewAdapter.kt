package com.example.quiz6

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.ViewTarget
import kotlinx.android.synthetic.main.recycler_view_layout.view.*

class RecyclerViewAdapter(private val items: MutableList<ItemModel>) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recycler_view_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size

    private lateinit var model: ItemModel

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            model = items[adapterPosition]
            itemView.imageView.setImageResource(
                Glide.with(itemView.context).load(model.imgUrl).into(itemView.imageView)
            )
            itemView.titleTextView.text = model.title
            itemView.descTextView.text = model.desc
        }
    }
}

private fun ImageView.setImageResource(into: ViewTarget<ImageView, Drawable>) {

}

